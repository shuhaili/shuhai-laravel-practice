<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome2');
});

Route::get('/qualification', function () {
    return view('qualification');
});

Route::get('/role', function () {
    return view('role');
});

Route::get('/title', function () {
    return view('title');
});

Route::get('/suffix', function () {
    return view('suffix');
});

Route::get('/student', function () {
    return view('student');
});
