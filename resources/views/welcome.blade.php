<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex,nofollow">
        <title>WASP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" type="image/ico" href="/images/favicon.ico"/>

        <!-- bootstrap and custom css respectively -->
        <link href="{{ URL::asset('css/base_bootstrap.css') }}" media="screen" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/base_layout.css') }}" media="screen" rel="stylesheet" type="text/css">

        <!-- add one of the jQWidgets styles -->
        <link rel="stylesheet" href="{{ URL::asset('jqwidgets/styles/jqx.base.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('jqwidgets/styles/jqx.darkblue.css') }}" type="text/css" />

        <!--- Bootstrap dashboard css --->

        <link href="{{ URL::asset('css/sb-admin-2.min.css') }}" media="screen" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/metisMenu.min.css') }}" media="screen" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/font-awesome.min.css') }}" media="screen" rel="stylesheet" type="text/css">


        <!--Custom css for the page -->
        <link href="{{ URL::asset('css/custom.css') }}" media="screen" rel="stylesheet" type="text/css">
        
        <!-- add the jQuery and jQWidgets script -->
        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxcore.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxdata.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxbuttons.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxscrollbar.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxdropdownlist.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxlistbox.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxmenu.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxribbon.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxgrid.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxgrid.pager.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxgrid.sort.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxgrid.selection.js') }}"></script><script type="text/javascript" src="{{ URL::asset('demos.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxgrid.columnsresize.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxdate.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxscheduler.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxscheduler.api.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxdatetimeinput.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxcalendar.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxtooltip.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxwindow.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxtabs.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxcheckbox.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxnumberinput.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxradiobutton.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxinput.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxdatatable.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxdraw.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/jqxchart.core.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/globalization/globalize.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('jqwidgets/globalization/globalize.culture.de-DE.js') }}"></script>


        <!--- javscript dependencies for bootstrap dashboard -->
        <script type="text/javascript" src="{{ URL::asset('js/sb-admin-2.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/metisMenu.min.js') }}"></script>

        <!-- Javascript dependency for bootstrap -->
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
        @yield('jshere')
        <!-- add custom js -->
        <script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>
    </head>
    <body>
    @yield('test')
    <div id="logo">
        <div class="main">
            <div id="headerLogo">
                <a href="/" title="WASP Home">
                    <img src="{{ URL::asset('/images/layout/header_logo_2014.png') }}" alt="ETSU"
                         width="298"
                         height="72"/>
                </a>
            </div>
            <div id="headerTitle">
                <h3>Child Study Center</h3>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <!-- Header End -->

    <!-- Navigation Start -->
    <div class="nav-fixed">
        <nav id="nav-wrap">
            <div class="main">
                <div id="menu-icon">
                    <div>Main Navigation</div>
                </div>
                <ul id="nav">
                </ul>
                <div class="clear"></div>
            </div>
        </nav>
    </div>
    <!-- Navigation End -->

    <div id="main" class="container-fluid">
        <div class="row">

                    <div role="navigation" class="navbar-default sidebar">
                        <div class="sidebar-nav navbar-collapse">
                            <ul id="side-menu" class="nav in">
                                <li>Student Dashboard</li>
                                <li>My application</li>
                                <li>My availablility</li>
                                <li>My profile</li>    
                                <li>Logout</li>
                            </ul>
                        </div>
                    </div>
            @if (Request::path() == '/')
                @yield('page')
            @else
                <div id="page-wrapper">
                    @yield('page')
                </div>
            @endif



        </div>
    </div>


        <!-- Footer Start -->
        <footer>
        <!--
            <div id="footerMountains">
                <img src="{{ URL::asset('/images/layout/mountains.png') }}" alt="Mountains"/>
            </div>
        -->
            <div class="main">
                <div id="footerInfoOne">
                    <p style="padding-top: 20px;">East Tennessee State University<br/>
                        PO BOX 70300 | Johnson City, TN 37614<br/>
                        423-439-1000 | <a href="mailto:info@etsu.edu">info@etsu.edu</a>
                    </p>
                </div>
                <div id="footerInfoTwo">
                    <p style="padding-top: 20px;">Child Study Center<br/>2101 Signal Drive<br/>Johnson City, TN 37614<br/>Email: <a href="mailto:childstudycenter@etsu.edu">childstudycenter@etsu.edu</a><br/>Phone: 423-439-4888 | Fax: 423-439-7561</p>
                </div>
            </div>
            <div class="main clear">
                <div class="privacy">
                    <span>East Tennessee State University</span>
                    &nbsp; &copy;&nbsp; <?php echo date('Y'); ?>&nbsp; all rights reserved &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="http://www.etsu.edu/etsuhome/documents/webprivacystatement.pdf">privacy policy</a>
                </div>
            </div>
        </footer>
        <!-- Footer End -->



    </body>
</html>